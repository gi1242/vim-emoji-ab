" Vim plugin to conditionally expand abbreviations on a matching prefix.
" Maintainer:	GI <gi1242+vim@nospam.com> (replace nospam with gmail)
" Created:	Sun 11 Aug 2019 11:55:39 AM EDT
" Last Changed:	Mon 16 Sep 2024 04:09:45 PM EDT
" Version:	0.1
"
" Description:
"   TODO: Digraphs for standard smileys

let s:digraphs = {
	\ ':)'	    : ['😄', 'smiley'],
	\ ':('	    : ['🙁', 'slightly_frowning_face'],
	\ ';)'	    : ['😉', 'wink'],
	\ ':$'	    : ['😊', 'blush'],
	\ ':D'	    : ['😁', 'grin'],
	\ '8\|'	    : ['😳', 'flushed'],
	\ ':O'	    : ['😲', 'astonished'],
	\ '8O'	    : ['😲', 'astonished'],
	\ ':P'	    : ['😛', 'stuck_out_tongue'],
	\ ';P'	    : ['😜', 'stuck_out_tongue_winking_eye'],
	\ 'XP'	    : ['😝', 'stuck_out_tongue_closed_eyes'],
	\ '%)'	    : ['😖', 'confounded'],
	\ '%/'	    : ['😕', 'confused'],
	\ '8)'	    : ['😎', 'sunglasses'],
	\ 'B)'	    : ['😎', 'sunglasses'],
	\ ';('	    : ['😢', 'cry'],
	\ ':\|'	    : ['😑', 'expressionless'],
	\ ':<'	    : ['😡', 'rage'],
	\ ':/'	    : ['😒', 'unamused'],
	\ '\|O'	    : ['😴', 'sleeping'],
	\ '8/'	    : ['🙄', 'face_with_rolling_eyes'],
	\ ':X'	    : ['🤐', 'zipper_mouth_face'],
\ }


for [s:ab, s:em] in items( s:digraphs )
    exec ':digraph' s:ab char2nr(s:em[0])
endfor

unlet s:ab s:em s:digraphs
